
package clases;

/**
 *
 * @author eperez
 */
public class suplidor {
    int id;
    String suplidor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSuplidor() {
        return suplidor;
    }

    public void setSuplidor(String suplidor) {
        this.suplidor = suplidor;
    }
    
}
