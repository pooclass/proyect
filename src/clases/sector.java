
package clases;

/**
 *
 * @author eperez
 */
public class sector {
    int id;
    String sector;
    provincia pro = new provincia();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSector() {
        return sector;
    }

    public void setSector(String sector) {
        this.sector = sector;
    }

    public provincia getPro() {
        return pro;
    }

    public void setPro(provincia pro) {
        this.pro = pro;
    }
    
}
