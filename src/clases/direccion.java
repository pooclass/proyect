
package clases;

/**
 *
 * @author eperez
 */
public class direccion {
    String direccion;
    pais pas = new pais();
    provincia pro = new provincia();
    sector sec = new sector();

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public pais getPas() {
        return pas;
    }

    public void setPas(pais pas) {
        this.pas = pas;
    }

    public provincia getPro() {
        return pro;
    }

    public void setPro(provincia pro) {
        this.pro = pro;
    }

    public sector getSec() {
        return sec;
    }

    public void setSec(sector sec) {
        this.sec = sec;
    }
}
