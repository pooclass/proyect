
package clases;

/**
 *
 * @author eperez
 */
public class modelo {
    int id;
    String modelo;

marca mar = new marca();  

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public marca getMar() {
        return mar;
    }

    public void setMar(marca mar) {
        this.mar = mar;
    }
    
}
