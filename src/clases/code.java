/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecttaller.Ent_inv;

/**
 *
 * @author Eduardo Liz
 */
public class code {
    
    public static String codigo() {
        
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.inventario);
        BufferedReader read;
        
        String codigo = "";
        String nxtLine = "";
        
        try {
            if(archivo.exists()) {
                read = new BufferedReader(new FileReader(archivo));
                
                while((nxtLine = read.readLine()) != null){
                    String[] Datos = nxtLine.split(",");
//                    System.out.println(Datos[0]);
                    codigo = Datos[0];
                }                
                read.close();
                
                if(codigo.equals("")){
                    codigo = "1";
                }
                else{
                    int code = Integer.parseInt(codigo);
                    code ++;
                    codigo = String.valueOf(code);
                }                
            } else {
                codigo = "1";                
            }
            
        }
        catch (IOException ex){
            Logger.getLogger(Ent_inv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return codigo;
    }
}
