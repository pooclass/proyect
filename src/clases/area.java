
package clases;

/**
 *
 * @author eperez
 */
public class area {
    int id;
    String area;
    
    vehiculo veh = new vehiculo();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public vehiculo getVeh() {
        return veh;
    }

    public void setVeh(vehiculo veh) {
        this.veh = veh;
    }
}
