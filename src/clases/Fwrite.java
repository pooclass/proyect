/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import proyecttaller.Admision1;
import proyecttaller.Ent_inv;
import proyecttaller.Marcas;

/**
 *
 * @author Eduardo Liz
 */
public class Fwrite {

    public static void Awrite(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j, String k){
        
//        String ruta = "/home/eperez/NetBeansProjects/admision.txt";
//        String ruta = "C:\\Users\\Eduardo Liz\\Documents\\NetBeansProjects\\ProyectTaller\\files\\admision.txt";
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.admision);
        BufferedWriter bw;
        
        cliente client = new cliente();
        vehiculo ve = new vehiculo();
        
        client.setNombre(a);
        client.setApellido(b);
        client.setTelefono(c);
        client.setIdentificacion(d);
        client.setTipo_id(e);
        client.setDireccion(f);
        ve.setTipo(g);
        ve.setMarca(h);
        ve.setModelo(i);
        ve.setChasis(j);
        
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
                bw.write("\r\n" + client.getNombre() +";");
                bw.write(client.getApellido() +";");
                bw.write(client.getTelefono() +";");
                bw.write(client.getIdentificacion() +";");
                bw.write(client.getTipo_id()+";");
                bw.write(client.getDireccion() +";");
                bw.write(ve.getTipo() +";");
                bw.write(ve.getMarca() +";");
                bw.write(ve.getModelo() +";");
                bw.write(ve.getChasis() +";");
                bw.write(k);
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(client.getNombre() +";");
                bw.write(client.getApellido() +";");
                bw.write(client.getTelefono() +";");
                bw.write(client.getIdentificacion() +";");
                bw.write(client.getTipo_id()+";");
                bw.write(client.getDireccion() +";");
                bw.write(ve.getTipo() +";");
                bw.write(ve.getMarca() +";");
                bw.write(ve.getModelo() +";");
                bw.write(ve.getChasis() +";");
                bw.write(k);
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(Admision1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void Awrite(String a){
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.marca);
        BufferedWriter bw;
        
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
                bw.write("\r\n"+ a);
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write("\r\n"+ a);
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(Marcas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void Awrite(String a, String b, String c){
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.inventario);
        BufferedWriter bw;
        
        Inventario inv = new Inventario();
        inv.setDesc(a);
        inv.setCant(b);
        inv.setPrecio(c);
        
        String codigo = code.codigo();
        
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
                bw.write("\r\n"+ codigo + ",");
                bw.write(inv.getDesc() + ",");
                bw.write(inv.getCant() + ",");
                bw.write(inv.getPrecio());
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(codigo + ",");
                bw.write(inv.getDesc() + ",");
                bw.write(inv.getCant() + ",");
                bw.write(inv.getPrecio());
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(Ent_inv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void Fupdate(String a, String temp){
        filePath ruta = new filePath();
        String nxtLine = "";
        int i = 1;

        try{
            BufferedReader read = new BufferedReader(new FileReader(ruta.marca));
            BufferedWriter bw = new BufferedWriter(new FileWriter(ruta.temp));

            while(nxtLine != null){
            nxtLine = read.readLine();
            
            if(nxtLine != null){
                if(!nxtLine.equals(temp)){
                    if(i == 1){
                        bw.write(nxtLine); 
                        i = 0;
                    }
                    else
                        bw.write("\r\n" + nxtLine); 
                }
                else{
                    if(i == 1){
                        bw.write(a);
                        i = 0;
                    }
                    else
                        bw.write("\r\n" + a); 
                }
            }
        }
        read.close();
        bw.close();
        
        }catch(IOException ex){
            Logger.getLogger(Admision1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Path path = Paths.get(ruta.marca);
        try {
            Files.delete(path);
        } catch (IOException ex) {
            Logger.getLogger(Fwrite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        path = Paths.get(ruta.temp);
        try {
            Files.move(path, path.resolveSibling("marca.txt"));
        } catch (IOException ex) {
            Logger.getLogger(Fwrite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static boolean identifyuser(String u, String p){
        
        filePath ruta = new filePath();
        String nxtLine;
        String user;
        String pass;
        
        try {
                BufferedReader read = new BufferedReader(new FileReader(ruta.user));
                
                while((nxtLine = read.readLine()) != null){
                    String[] Datos = nxtLine.split(",");
//                    System.out.println(Datos[0]);
                    user = Datos[0];
                    pass = Datos[1];
                    
                    if(Datos[0].equals(u) && Datos[1].equals(p)){
                        read.close();
                        return true;
                    }
                }                
                read.close();
            
        }
        catch (IOException ex){
            Logger.getLogger(Ent_inv.class.getName()).log(Level.SEVERE, null, ex);
        }
                return false;        
    }
    
    public static void Awrite(String a, String b){
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.modelo);
        BufferedWriter bw;
        
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
                bw.write("\r\n"+ a + ",");
                bw.write(b + ",");
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(a + ",");
                bw.write(b);
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(Ent_inv.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void Fupdate(String a, String b, String temp, String temp1){
        filePath ruta = new filePath();
        String nxtLine = "";
        int i = 1;

        try{
            BufferedReader read = new BufferedReader(new FileReader(ruta.modelo));
            BufferedWriter bw = new BufferedWriter(new FileWriter(ruta.temp));

//            System.out.println(b);
            
            while((nxtLine = read.readLine()) != null){
                String[] Datos = nxtLine.split(",");
                
                if(!Datos[0].equals(temp) && !Datos[1].equals(temp1)){
                    if(i == 1){
                        bw.write(Datos[0] + ",");
                        bw.write(Datos[1]); 
                        i = 0;
                    }
                    else{
                        bw.write("\r\n" + Datos[0] + ",");
                        bw.write(Datos[1]);  
                    }
                }
                else{
                    if(i == 1){
                        bw.write(a + ",");
                        bw.write(b);
                        i = 0;
                    }
                    else{
                        bw.write("\r\n" + a + ",");
                        bw.write(b); 
                    }
                }
            }
            read.close();
            bw.close();
        
        }catch(IOException ex){
            Logger.getLogger(Admision1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Path path = Paths.get(ruta.modelo);
        try {
            Files.delete(path);
        } catch (IOException ex) {
            Logger.getLogger(Fwrite.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        path = Paths.get(ruta.temp);
        try {
            Files.move(path, path.resolveSibling("modelo.txt"));
        } catch (IOException ex) {
            Logger.getLogger(Fwrite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void Awrite(String a, String b, String c, String d, String e, String f){
        
//        String ruta = "/home/eperez/NetBeansProjects/admision.txt";
//        String ruta = "C:\\Users\\Eduardo Liz\\Documents\\NetBeansProjects\\ProyectTaller\\files\\admision.txt";
        filePath ruta = new filePath();
        
        File archivo = new File(ruta.user);
        BufferedWriter bw;
        
        cliente client = new cliente();
        vehiculo ve = new vehiculo();
                        
        try {
            if(archivo.exists()) {
                bw = new BufferedWriter(new FileWriter(archivo, true));
                bw.write("\r\n" + a +";");
                bw.write(b +",");
                bw.write(c +",");
                bw.write(d +",");
                bw.write(e+",");
                bw.write(f +",");
            } else {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(a +",");
                bw.write(b +",");
                bw.write(c +",");
                bw.write(d +",");
                bw.write(e +",");
                bw.write(f +",");
            }
            bw.close();
        }
        catch (IOException ex){
            Logger.getLogger(Admision1.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
